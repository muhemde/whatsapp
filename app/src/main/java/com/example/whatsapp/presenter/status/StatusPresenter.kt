package com.example.whatsapp.presenter.status

import com.example.whatsapp.data.StatusData

class StatusPresenter(val bindViewStatus: StatusBindView) {

    fun getStatusData(){
        val dataStatus = StatusData
        bindViewStatus.onSuccess("Success Bind Status Data", dataStatus.listData)
    }
}