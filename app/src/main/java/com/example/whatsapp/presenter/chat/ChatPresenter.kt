package com.example.whatsapp.presenter.chat

import com.example.whatsapp.data.ChatData

class ChatPresenter(val bindView: ChatBindView) {

    fun getData(){
        val dataChat = ChatData

        bindView.onSuccessBind("Success Bind Data", dataChat.listData)
    }
}