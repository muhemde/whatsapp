package com.example.whatsapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Status(
    var avatar: Int = 0,
    var name: String? = "",
    var time: String? = ""
) : Parcelable