package com.example.whatsapp.data

import com.example.whatsapp.R
import com.example.whatsapp.model.Chat
import com.example.whatsapp.model.Status

object StatusData {
    private val image: Array<Int> =
        arrayOf(
            R.drawable.user1,
            R.drawable.user2,
            R.drawable.user3,
            R.drawable.user4,
            R.drawable.user5,
            R.drawable.user6,
            R.drawable.user7,
            R.drawable.user8,
            R.drawable.user9,
            R.drawable.user10
        )

    private val name: Array<String> =
        arrayOf(
            "Andi",
            "Indra Brookman",
            "Jackie Chan",
            "The Rock",
            "SPG Oli",
            "Brother",
            "Eminem",
            "Younglex",
            "Sugar Dedi",
            "Adam Levine"
        )


    private val time: Array<String> =
        arrayOf(
            "Hari ini 07.43",
            "Hari ini 07.32",
            "Hari ini 07.00",
            "Kemarin 17.00",
            "Kemarin 16.00",
            "Kemarin 15.00",
            "Kemarin 14.00",
            "Kemarin 13.00",
            "Kemarin 12.00",
            "Kemarin 11.00"
        )


    val listData: ArrayList<Status>
        get() {
            val list : ArrayList<Status> = arrayListOf<Status>()
            for (i in image.indices){
                val status = Status()
                status.avatar = image[i]
                status.name = name[i]
                status.time = time[i]
                list.add(status)
            }
            return list
        }

}
