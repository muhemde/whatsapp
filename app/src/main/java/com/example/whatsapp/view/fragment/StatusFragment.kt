package com.example.whatsapp.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.whatsapp.R
import com.example.whatsapp.adapter.StatusAdapter
import com.example.whatsapp.model.Status
import com.example.whatsapp.presenter.status.StatusBindView
import com.example.whatsapp.presenter.status.StatusPresenter
import kotlinx.android.synthetic.main.fragment_status.*

class StatusFragment : Fragment(), StatusBindView {
    private var presenterStatus : StatusPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_status, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenterStatus = StatusPresenter(this)
        presenterStatus?.getStatusData()
    }

    override fun onSuccess(msg: String, status: List<Status?>?) {
        rv_status.apply {
            adapter = StatusAdapter(status as List<Status>)
            setHasFixedSize(true)
        }
    }
}