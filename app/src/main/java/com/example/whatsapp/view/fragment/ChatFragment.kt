package com.example.whatsapp.view.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.whatsapp.R
import com.example.whatsapp.adapter.ChatAdapter
import com.example.whatsapp.model.Chat
import com.example.whatsapp.presenter.chat.ChatBindView
import com.example.whatsapp.presenter.chat.ChatPresenter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment() : Fragment(), ChatBindView {
    private var presenter : ChatPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = ChatPresenter(this)
        presenter?.getData()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater!!.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item!!.itemId

        if (id == R.id.new_grup) {
            Snackbar.make(chat_Container, "New Group", Snackbar.LENGTH_LONG).show()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onSuccessBind(msg: String, chat: List<Chat?>?) {
        rv_chat.apply {
            adapter = ChatAdapter(chat as List<Chat>)
            setHasFixedSize(true)
        }
    }

}