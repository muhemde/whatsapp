package com.example.whatsapp.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.whatsapp.R
import com.example.whatsapp.adapter.CallAdapter
import com.example.whatsapp.model.Calls
import com.example.whatsapp.presenter.calls.CallBindView
import com.example.whatsapp.presenter.calls.CallPresenter
import kotlinx.android.synthetic.main.fragment_calls.*

class CallsFragment : Fragment(), CallBindView {

    private var presenter : CallPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_calls, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = CallPresenter(this)
        presenter?.getCallData()
    }

    override fun onSuccessBind(msg: String, chat: List<Calls?>?) {
        rv_call.apply {
            adapter = CallAdapter(chat as List<Calls>)
            setHasFixedSize(true)
        }
    }
}