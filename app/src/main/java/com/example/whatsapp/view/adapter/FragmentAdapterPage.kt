package com.example.whatsapp.view.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.whatsapp.view.fragment.CallsFragment
import com.example.whatsapp.view.fragment.ChatFragment
import com.example.whatsapp.view.fragment.StatusFragment

class FragmentAdapterPage(fm: FragmentManager): FragmentPagerAdapter(fm) {

    private val pages = listOf(
        ChatFragment(),
        StatusFragment(),
        CallsFragment()
    )
    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Chat"
            1 -> "Status"
            else -> "Calls"
        }
    }
}